[Original repository at GitHub by Tsubajashi](https://github.com/Tsubajashi/mpv-settings)  
hi tshbajashi, hope you dont mind i kinda adjusted them for myself. but I do hope this mostly untouched (except this line and a few breaks)
will help people i share these altered configs with.

# mpv-settings
basically my settings for MPV (Windows/Linux Compatible) (i think)

thats my tweaked config.
might not be suitable for your pc, but if you have any recommendations to switch stuff, just tell me.
Discord: Tsubajashi#8355
**NOTE: Do not contact Tsubajashi if you want to get help with my fork of the configs. You can contact me instead through me@katou.pw (rarely checked) or at Discord: katoumegumi_#3231**  

or just make a pull request if you have any recommendations, if i like it, i put it in.

# Installation
Depending on your Operating System, you need to place the stuff inside the zip in a certain directory.
The root directory needs to look like this:

->mpv

-->input.conf

-->mpv.conf

--->Shaders

--->script-opts

--->scripts


# Windows Path
"C:\Users\YOURNAME\AppData\Roaming\mpv"

# Linux Path
/home/USERNAME/.config/mpv